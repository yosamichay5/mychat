/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server;

import Listeners.ServerListener;
import Main.Const;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Yos Amichay
 * this class present the sevr side in the chat
 */
public class ServerThread extends Thread {

    ServerSocket server;
    ServerFrame serverFrame;
    Socket socket = null;
    ServerListener listener;
    Vector<ServerListener> listeners;

    public ServerThread(ServerFrame frame) {
        try {
            server = new ServerSocket(Const.port);
           
            serverFrame = frame;
            listeners = new Vector<ServerListener>();
        } catch (IOException ex) {
            Logger.getLogger(ServerThread.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
/**
 * the run function start the open the socket and start to listen to it
 */
    public void run() {
        try {
            serverFrame.getServerTextArea().append("server connect \n");
            while (true) {
                 
                socket = server.accept();
                listener = new ServerListener(listeners, socket, serverFrame);
                listeners.add(listener);
                listener.start();

            }
        } catch (IOException ex) {
            Logger.getLogger(ServerThread.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    /**
     *when the sever press in the stop button the Server Thread disconnect from the socket 
     */
    void disconnect() {
        for (ServerListener l : listeners) {
         l.disconnect();
        }
    }

}
