/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;

 

/**
 *
 * @author Yos Amichay
 * this class hold the const in this project
 * @param port the socket port
 * @param local_host the server ip
 */
public class Const {
    public static final int port = 35356;
    public static final String local_host = "127.0.0.1";
    
}
