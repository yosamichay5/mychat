/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Client;

import Listeners.ClientListener;
import Main.Const;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author amichay
 */
public class ClientThread extends Thread {

    String name;
    Socket socket;
    ClientFrame frame;
    ClientListener clientListener;

    public ClientThread(String name, ClientFrame frame) {
        try {
            this.frame = frame;
            this.name = name;
            socket = new Socket(Const.local_host, Const.port);
        } catch (IOException ex) {
            Logger.getLogger(ClientThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void run() {
        clientListener = new ClientListener(name, frame, this, socket);
        clientListener.start();
    }

    public void getUsersNames() {
        clientListener.getUsersNames();
    }

    public void sendMsgTOAll(String text) {
        clientListener.sendMsgTOAll(text);
    }

    public void sendMsgToUser(String text, String to) {
        clientListener.sendMsgToUser(text, to);
    }

    void closeUser() {
        clientListener.closeUser();
    }

}
