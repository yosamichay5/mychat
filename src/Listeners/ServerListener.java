/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listeners;

import Server.ServerFrame;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author amichay
 */
public class ServerListener extends Thread {

    DataInputStream in;
    PrintWriter out;
    Socket socket;
    String input = "";
    ServerFrame frame;
    Vector<ServerListener> listeners;
    String name;
/**
 * constructor 
 * @param listeners pointer to vector that holds the server's listeners
 * @param socket pointer to the socket ib this chat
 * @param frame pointer to the server fraame
 */
    public ServerListener(Vector<ServerListener> listeners, Socket socket, ServerFrame frame) {
        this.socket = socket;
        this.frame = frame;
        this.listeners = listeners;

    }
/**
 * the listening to the clients
 * contain the protocol of the language, the input and the output
 */
    public void run() {
        try {
            in = new DataInputStream((new BufferedInputStream(socket.getInputStream())));
            out = new PrintWriter(socket.getOutputStream());
            System.out.println("dd");
            input = in.readLine();
            StringTokenizer tokens = new StringTokenizer(input, " ");
            if (tokens.nextToken().equals("connect")) {
                name = tokens.nextToken();
                String text = name + " connected";
                frame.getServerTextArea().append(text + "\n");
                for (ServerListener l : listeners) {
                    l.sendToMyUser(text);
                }
                
            }
            while (true) {
                input = in.readLine();
                StringTokenizer t = new StringTokenizer(input, " ");
                String[] data = new String[t.countTokens()];
                int index = 0;
                while (t.hasMoreTokens()) {
                    data[index] = t.nextToken();
                    ++index;
                }

                if ((input == null) || (data == null)) {
                    continue;
                }
                if (data[1].equals("disconnect")) {
                    String text = data[0] + ": disconnected";
                    frame.getServerTextArea().append(text + "\n");
                    out.close();
                    sendMsgToUsers(text);
                    frame.getServerTextArea().append(text + "\n");
                    continue;
                }

                if (input.equals("get users")) {
                    sendUsersNames();
                    continue;
                }
                if (data[0].equals("set_msg")) {
                    String userName = data[2];
                    String text = "";
                    for (int i = 3; i < data.length; i++) {
                        text += " " + data[i];
                    }
                    sendMsgToAnother(text, userName);
                    continue;

                }
                if (data[0].equals("set_msg_all")) {
                    String text = "";
                    for (int i = 3; i < data.length; i++) {
                        text += " " + data[i];
                    }
                    sendMsgToUsers(name + ":" + text);
                    continue;
                }

            }
        } catch (Exception ex) {
            Logger.getLogger(ServerListener.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
/**
 * send msg from the server (or from one client) to all users
 * @param text the msg need to been send
 */
    public void sendMsgToUsers(String text) {
        frame.getServerTextArea().append(text + "\n");
        for (ServerListener l : listeners) {
            l.sendToMyUser("msg_from " + text);
        }
    }
/**
 * the listener send msg to his client
 * @param text the msg need to been send
 */
    public void sendToMyUser(String text) {
        out.println(text);
        out.flush();
    }
/**
 * the listener send the list of all chat users
 */
    public void sendUsersNames() {
        String users_names = getUsersName();
        users_names = "users_list: " + users_names;
        users_names += " end";
        out.println(users_names);
        out.flush();
    }
/**
 * help function, 
 * @return the list of all chat users
 */
    public String getUsersName() {
        String users_names = "[";
        for (ServerListener l : listeners) {
            users_names += l.name + ",";
        }
        users_names += "]";
        return users_names;
    }
/**
 * the server or the listener's client send a msg to another client
 * @param text the msg need to been send
 * @param userName the name of the user who need to get the msg
 */
    public void sendMsgToAnother(String text, String userName) {
        String msg = "";
        for (ServerListener l : listeners) {
            if (l.name.equals(userName) && !l.name.equals(name)) {
                msg = "msg_from " + text;
                l.sendToMyUser(msg);
                break;
            }
        }
        if (msg.equals("")) {
            sendToMyUser(userName + " is not a user");
        } else {
            sendToMyUser("me" + " : " + text);
        }
    }
/**
 * close the socket from the server side
 */
    public void disconnect() {
        sendToMyUser("server disconnected");
        out.close();
    }

}
