/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listeners;

import Client.ClientFrame;
import Client.ClientThread;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author amichay
 */
public class ClientListener extends Thread {

    DataInputStream in;
    PrintWriter out;
    String clientName;
    ClientFrame frame;
    ClientThread client;
    Socket socket;
    

    public ClientListener( String name, ClientFrame frame, ClientThread client ,Socket socket) {
        
        this.clientName = name;
        this.frame = frame;
        this.client = client;
        this.socket = socket;
    }

    public void run() {
        try {
            in = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream());
            String input = "";
            System.out.println("dd");
            out.println("connect " + clientName);
            out.flush();
            while (true) {
                input = in.readLine();
                StringTokenizer tokens = new StringTokenizer(input, " ");
                String [] data = new String[tokens.countTokens()];
                int index = 0;
                while (tokens.hasMoreTokens()) {
                    data[index] = tokens.nextToken();
                    System.out.println(data[index]);
                    index++;
                }
                if(input == null)continue;
                if (data[1].equals("disconnected")) {
                    String text = data[0] + " disconnected";
                    frame.getClientTextArea().append(text + "\n");
                    if (data[0].equals("server")) out.close();
                }
                if (data[1].equals("connected")) {
                    String text = data[0] + " connected";
                    frame.getClientTextArea().append(text + "\n");
                }
                if(data[0].equals("users_list:")){
                    frame.getClientTextArea().append(input + "\n");
                }
                if(data[0].equals("msg_from")){
                    frame.getClientTextArea().append(input + "\n");
                }
                if(data[0].equals("me")){
                    frame.getClientTextArea().append(input + "\n");
                }
                if(data[data.length-1].equals("user")){
                   // String text = input.substring(10);
                    frame.getClientTextArea().append(input + "\n");
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(ClientListener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     public void getUsersNames() {
        out.println("get users");
        out.flush();
    }

    public void sendMsgTOAll(String text) {
        out.println("set_msg_all : " + text);
        out.flush();
    }

    public void sendMsgToUser(String text, String to) {
        out.println("set_msg : " + to +" "+ text);
        out.flush();
    }

    public void closeUser() {
        out.println(clientName + " disconnect");
        out.flush();
        out.close();
    }

}
